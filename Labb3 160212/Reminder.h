//
//  Reminder.h
//  Labb3 160212
//
//  Created by Christian on 2016-02-12.
//  Copyright © 2016 Christian Heina. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Reminder : NSObject <NSCoding>
@property (nonatomic) NSString *reminderTitle;
@property (nonatomic) NSString *reminderDescription;
@property (nonatomic) NSDate *reminderCreateDate;
@property (nonatomic) NSString *reminderInfoText;

+(void)saveArrayWithReminders:(NSArray*)array;
+(NSArray*)loadArrayWithReminders;

@end
