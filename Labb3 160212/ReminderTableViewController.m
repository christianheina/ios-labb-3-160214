//
//  ReminderTableViewController.m
//  Labb3 160212
//
//  Created by Christian on 2016-02-12.
//  Copyright © 2016 Christian Heina. All rights reserved.
//

#import "ReminderTableViewController.h"
#import "ReminderDetailViewController.h"
#import "AddReminderViewController.h"
#import "Reminder.h"
#import "CustomTableViewCell.h"

@interface ReminderTableViewController ()
@property (nonatomic) NSMutableArray *reminders;

@end

@implementation ReminderTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated{
    self.reminders = [Reminder loadArrayWithReminders].mutableCopy;
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.reminders.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CustomTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ReminderCell"
                                                            forIndexPath:indexPath];
    
    Reminder *r = self.reminders[indexPath.row];
    cell.customTitle.text = r.reminderTitle;
    cell.customSubtitle.text = r.reminderDescription;
    NSString *date = [NSDateFormatter localizedStringFromDate:r.reminderCreateDate
                                                    dateStyle:NSDateFormatterShortStyle
                                                    timeStyle:NSDateFormatterNoStyle];
    cell.customDate.text = [NSString stringWithFormat:@"Created: %@", date];
    return cell;
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.reminders removeObjectAtIndex:indexPath.row];
        [Reminder saveArrayWithReminders:self.reminders];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"ReminderDetail"]){
        ReminderDetailViewController *destination = [segue destinationViewController];
        UITableViewCell *cell = sender;
        NSInteger index = [self.tableView indexPathForCell:cell].row;
        destination.reminder = self.reminders[index];
        
    } else if ([segue.identifier isEqualToString:@"ReminderAdd"]){
        AddReminderViewController *destination = [segue destinationViewController];
        destination.reminders = self.reminders;
    }
}

@end
