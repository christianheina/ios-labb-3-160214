//
//  AddReminderViewController.m
//  Labb3 160212
//
//  Created by Christian on 2016-02-12.
//  Copyright © 2016 Christian Heina. All rights reserved.
//

#import "AddReminderViewController.h"
#import "Reminder.h"

@interface AddReminderViewController () <UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UITextField *reminderTitleTextField;
@property (weak, nonatomic) IBOutlet UITextField *reminderDescriptionTextField;
@property (weak, nonatomic) IBOutlet UITextView *reminderInfoTextView;
@property (weak, nonatomic) IBOutlet UIButton *addButton;
@property (nonatomic) NSString *placeholder;

@end

@implementation AddReminderViewController

-(void)viewDidLoad{
    [self.reminderTitleTextField addTarget:self action:@selector(titleTextFieldChanged) forControlEvents:UIControlEventEditingChanged];
    [self toggleAddButton:NO];
    self.placeholder = self.reminderInfoTextView.text;
    self.reminderInfoTextView.delegate = self;
}

-(void)titleTextFieldChanged{
    if(![self.reminderTitleTextField.text isEqualToString:@""]){
        [self toggleAddButton:YES];
    } else {
        [self toggleAddButton:NO];
    }
}

-(void)toggleAddButton:(BOOL)enabled{
    self.addButton.enabled = enabled;
}

- (IBAction)addReminderButton:(id)sender {
    Reminder *reminder = [[Reminder alloc] init];
    reminder.reminderTitle = self.reminderTitleTextField.text;
    reminder.reminderDescription = self.reminderDescriptionTextField.text;
    reminder.reminderCreateDate = [NSDate date];
    if(![self.reminderInfoTextView.text isEqualToString:self.placeholder]){
        reminder.reminderInfoText = self.reminderInfoTextView.text;
    } else {
        reminder.reminderInfoText = @"";
    }
    [self.reminders insertObject:reminder atIndex:0];
    [Reminder saveArrayWithReminders:self.reminders];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)cancelButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)textViewDidBeginEditing:(UITextView *)textView{
    if([textView.text isEqualToString:self.placeholder]){
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
    [textView becomeFirstResponder];
}

-(void)textViewDidEndEditing:(UITextView *)textView{
    if([textView.text isEqualToString:@""]){
        textView.text = self.placeholder;
        textView.textColor = [UIColor lightGrayColor];
    }
    [textView resignFirstResponder];
}

@end
