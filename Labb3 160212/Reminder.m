//
//  Reminder.m
//  Labb3 160212
//
//  Created by Christian on 2016-02-12.
//  Copyright © 2016 Christian Heina. All rights reserved.
//

#import "Reminder.h"

@implementation Reminder

-(void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.reminderTitle forKey:@":reminderTitle"];
    [aCoder encodeObject:self.reminderDescription forKey:@":reminderDescription"];
    [aCoder encodeObject:self.reminderCreateDate forKey:@":reminderCreateDate"];
    [aCoder encodeObject:self.reminderInfoText forKey:@":reminderInfoText"];
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super init];
    if (self){
        self.reminderTitle = [aDecoder decodeObjectForKey:@":reminderTitle"];
        self.reminderDescription = [aDecoder decodeObjectForKey:@":reminderDescription"];
        self.reminderCreateDate = [aDecoder decodeObjectForKey:@":reminderCreateDate"];
        self.reminderInfoText = [aDecoder decodeObjectForKey:@":reminderInfoText"];
    }
    return self;
}

+(void)saveArrayWithReminders:(NSArray*)array{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:array];
    [defaults setObject:data forKey:@":saveReminderArray"];
    [defaults synchronize];
}

+(NSArray*)loadArrayWithReminders{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@":saveReminderArray"];
    if (!data){
        return @[];
    }
    NSArray *array = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    [defaults synchronize];
    return array;
}

@end
