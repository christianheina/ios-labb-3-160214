//
//  ReminderDetailViewController.m
//  Labb3 160212
//
//  Created by Christian on 2016-02-12.
//  Copyright © 2016 Christian Heina. All rights reserved.
//

#import "ReminderDetailViewController.h"

@interface ReminderDetailViewController ()
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *createDateLabel;
@property (weak, nonatomic) IBOutlet UITextView *infoTextView;

@end

@implementation ReminderDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = self.reminder.reminderTitle;
    self.descriptionLabel.text = self.reminder.reminderDescription;
    NSString *date = [NSDateFormatter localizedStringFromDate:self.reminder.reminderCreateDate
                                                    dateStyle:NSDateFormatterShortStyle
                                                    timeStyle:NSDateFormatterShortStyle];
    self.createDateLabel.text = [NSString stringWithFormat:@"Created: %@", date];
    self.infoTextView.text = self.reminder.reminderInfoText;
}

@end
