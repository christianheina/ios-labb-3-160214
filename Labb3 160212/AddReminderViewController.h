//
//  AddReminderViewController.h
//  Labb3 160212
//
//  Created by Christian on 2016-02-12.
//  Copyright © 2016 Christian Heina. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddReminderViewController : UIViewController
@property () NSMutableArray *reminders;

@end
