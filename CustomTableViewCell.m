//
//  CustomTableViewCell.m
//  Labb3 160212
//
//  Created by Christian on 2016-02-14.
//  Copyright © 2016 Christian Heina. All rights reserved.
//

#import "CustomTableViewCell.h"

@implementation CustomTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
