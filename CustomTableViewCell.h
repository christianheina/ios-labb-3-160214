//
//  CustomTableViewCell.h
//  Labb3 160212
//
//  Created by Christian on 2016-02-14.
//  Copyright © 2016 Christian Heina. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *customTitle;
@property (weak, nonatomic) IBOutlet UILabel *customSubtitle;
@property (weak, nonatomic) IBOutlet UILabel *customDate;

@end
